﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalBoosterHandler : MonoBehaviour
{
    private Rigidbody _rigidbody;

    public void Init(Rigidbody rigidbody)
    {
        _rigidbody = rigidbody;
    }

    private void LateUpdate()
    {
        CheckBooster();
    }

    private void CheckBooster()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 10, 1 << LayerMask.NameToLayer("Booster"))) // Layer == Oven
        {
            var oven = hit.collider.gameObject.GetComponent<VerticalBooster>();
            if (oven != null)
            {
                _rigidbody.velocity = _rigidbody.velocity = _rigidbody.velocity.x * (Vector3.right) + (oven.GetForce().y/(transform.position - hit.transform.position).y * _rigidbody.mass * Vector3.up);

            }
        }
    
    }
}
