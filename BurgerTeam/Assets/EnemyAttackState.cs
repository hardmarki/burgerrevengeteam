﻿using System.Collections.Generic;
using Controllers;
using Units;
using UnityEngine;

namespace FSM
{
    public class EnemyAttackState : BaseState
    {
        public override StateId Id => StateId.Attack;

        private static readonly List<EventID> EventIds =
            new List<EventID>()
                {EventID.OnDeath, EventID.OnIdle, EventID.OnMove, EventID.OnScared, EventID.OnUncontroll};

        public override List<EventID> InterruptEvents => EventIds;

        public override EventID TransitionEventId => EventID.OnAttack;

        protected readonly AnimationContoller _anim;

        private int _delayToAttack = 0;
        private int _timeToNext = 0;
        protected int TimeApply = 0;

        public EnemyAttackState(AnimationContoller anim) // + weapon
        {

        }

        public override void ClearData()
        {
            _delayToAttack = 0;
            _timeToNext = 0;
            TimeApply = 0;
        }

        protected override void StartState()
        {

            StartNewAttack();
        }

        protected virtual void StartNewAttack()
        {
            _timeToNext = 0;
            var time = 0;
            if (_anim != null)
            {
                //    time = _anim.PlayRandomAttack(_timeToNext.TickToFloatSeconds()).ToTick();
            }
            else
            {
                time = _timeToNext / 2;
            }

            TimeApply = Mathf.Min(time, _timeToNext);
        }

        protected override void InState()
        {
            if (_delayToAttack > 0) // is load weapon
            {
                _delayToAttack--;
                return;
            }

            if (_timeToNext == TimeApply)
            {
                // actual attack
            }

            if (_timeToNext == 0)
            {
                FinishAttack();
            }
            else
                _timeToNext--;
        }

        protected virtual void FinishAttack()
        {
            if(false)// no target to attack
            {
                PushEvent(EventID.OnIdle);
            }
            else
            {
                StartNewAttack();
            }
        }

        protected override void FinishState()
        {
            _timeToNext = 0;
            if (_anim != null)
            {
                //_anim.StopAttack();
            }
        }

    }
}

