﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private WeaponModel _weaponModel;
    
    // Call from state
    public void StartAttack()
    {
        // Play Weapon Fx
        Attack();
        
    }

    // Call from state
    public void StopAttack()
    {
        // PLay Weapon FX
    }

    private void Attack()
    {
        // Shoot
    }
}
