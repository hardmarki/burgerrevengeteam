﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoosterStatus
{ 
None,
Inited,
Delay,
Increment,
MaxVelocity,
Decrement
}

public class VerticalBooster : MonoBehaviour
{
    [SerializeField]private float CurrentForce;
     
    [SerializeField] private GameObject _fx;

    [SerializeField]private BoosterStatus Status = BoosterStatus.None;

    private float DelayBeforeStart;

    private float MinVelocity;
    private float MaxVelocity;

    private float IncrementTime;
    private float MaxVelocityTime;
    private float DecrementTime;

    public void SetUp(float delayBeforeStart, float maxVelocityTime)
    {
        MaxVelocityTime = maxVelocityTime;

        MinVelocity = OvenConstrant.MinVelocity;
        MaxVelocity = OvenConstrant.MaxVelocity;
        DelayBeforeStart = delayBeforeStart;
        IncrementTime = DecrementTime = (OvenConstrant.OvenWholeCycleTime - delayBeforeStart)/2 + Time.fixedDeltaTime;

        CurrentForce = MinVelocity;

        Status = BoosterStatus.Inited;
    }

    private float timer;
    private float deltaVelocity;
    public void StartBooster()
    {
        CurrentForce = MinVelocity;
        timer = DelayBeforeStart;
        deltaVelocity = (MaxVelocity - MinVelocity) / (IncrementTime / Time.fixedDeltaTime);
    }


    public Vector3 GetForce()
    {
        return CurrentForce * Vector3.up;
    }

    private void FixedUpdate()
    {
        if (Status == BoosterStatus.Inited || Status == BoosterStatus.None)
            return;
        if (timer > 0)
        {
            timer -= Time.fixedDeltaTime;
            if (Status == BoosterStatus.Delay)
            {
                return;
            }
            else if (Status == BoosterStatus.Increment || Status == BoosterStatus.Decrement)
            {
                CurrentForce += Mathf.Sign(Status - BoosterStatus.Increment) * deltaVelocity;
            }
        }
        if (timer <= 0)
        {
            if (Status != BoosterStatus.Decrement)
            {
                if (Status == BoosterStatus.Delay)
                {
                    timer = IncrementTime;
                }

                if (Status == BoosterStatus.Increment)
                {
                    timer = MaxVelocityTime;
                    CurrentForce = MaxVelocity;
                }

                if (Status == BoosterStatus.MaxVelocity)
                {
                    timer = DecrementTime;
                }

                Status += 1;
            }
            else
                Status = BoosterStatus.Inited;
        }
    }

    public void Activate(bool enable)
    {
        _fx.gameObject.SetActive(enable);
    }
}
