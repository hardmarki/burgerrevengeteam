﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvensManager : MonoBehaviour
{
    private List<VerticalBooster> _ovens = new List<VerticalBooster>();

    public void SetConfig(OvenConfig[] configs)
    {
    }

    public void Add(VerticalBooster b)
    {
        
        _ovens.Add(b);
    }

    public void ActiveAll(bool enable)
    {
        foreach (var ov in _ovens)
        {
            ov.Activate(enable);
        }
    }
}
