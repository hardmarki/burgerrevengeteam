﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using System;

[Serializable]
public struct OvenConfig
{ 
    public float DelayBeforeStart;
    public float MaxVelocityTime;
}

public static class OvenConstrant
{
    public static float OvenWholeCycleTime = 15f;
    public static float MaxVelocity = 5;
    public static float MinVelocity = 1;

    public static float MaxVelocityTime = 5f;
}

public class Scene_Oven : MonoBehaviour
{
    [SerializeField] private GameObject _ovenPrefab;

    [SerializeField] private Transform StartPoint;

    [SerializeField] private OnColliderAction _leftBlocker;
    [SerializeField] private OnColliderAction _rightBlocker;

    [SerializeField] private OnColliderAction _ovenTrigger;

    [SerializeField] private Text text;

    [SerializeField] private CinemachineVirtualCamera _camera;

    [SerializeField] private OvenConfig[] _OvensConfig;

    [SerializeField] private OvensManager OvenManager;

    [SerializeField] private int WholeCycleCount = 5;

    private void Start()
    {
        for (int i = 0; i < _OvensConfig.Length; i++)
        {
            OvenManager.Add( Instantiate(_ovenPrefab, StartPoint.transform.position + (-Vector3.up) + i * Vector3.right, Quaternion.identity).GetComponent<VerticalBooster>());
        }
        _rightBlocker.transform.parent.position = StartPoint.transform.position + (_OvensConfig.Length) * Vector3.right;
        SetBlockers();

        text.gameObject.SetActive(false);
    }

    private void SetBlockers()
    {
        _ovenTrigger.OnTrigerEnterInit(() => 
        { 
            _leftBlocker.Collider.isTrigger = false;
            _rightBlocker.Collider.isTrigger = false;

            OvenManager.ActiveAll(true);
            text.gameObject.SetActive(true);

            _camera.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = 10;

            timer = OvenConstrant.OvenWholeCycleTime * WholeCycleCount;
        });
    }

    private float timer;
    private void Update()
    {
        if (text.gameObject.activeSelf)
        {
            timer -= Time.deltaTime;
            if( int.Parse(text.text)  != (int) timer)
            text.text = ((int)(timer)).ToString();
        }
    }
}

