﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InventoryItems
{ 
    None = 0,
    Bacon = 1
}

public class Inventory : MonoBehaviour
{
    [SerializeField] private GameObject _bacon;

    public void SetItem(InventoryItems item , bool enable)
    {
        _bacon.SetActive(enable);
    }

    public bool Have(InventoryItems itemId)
    {
        return _bacon.activeSelf;
    }


    
}
