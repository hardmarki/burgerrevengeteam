﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    public class DeathState : BaseState
    {
        public override StateId Id => StateId.Death;

        private static readonly List<EventID> EventIds = new List<EventID>(){EventID.OnResurrect};

        public override List<EventID> InterruptEvents => EventIds;

        public override EventID TransitionEventId => EventID.OnDeath;

        private readonly Action _onStartDeath;
        private readonly Action _onFinishDeath;
        private readonly int _finishTime = 0;
        
        private int _time = 0;
        
        public DeathState(Action onStartDeath, Action onFinishDeath, int deadTime)
        {
            _onStartDeath = onStartDeath;
            _onFinishDeath = onFinishDeath;
            _time = _finishTime = deadTime;
        }

        protected override void StartState()
        {
            if (_time != _finishTime)
            { 
                Debug.LogWarning("Somting wrong, duplicate call");
                return;
            }
            base.StartState();
            _onStartDeath();
        }

        protected override void InState()
        {
            base.InState();
            if (_time-- == 0)
            {
                _onFinishDeath();
            } 
        }

        protected override void FinishState()
        {
            base.FinishState();
            _time = _finishTime;
        }

        public override void ClearData()
        {
            _time = _finishTime;
        }
    }
}