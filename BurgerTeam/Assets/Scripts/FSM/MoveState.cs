﻿using Controllers;
using Units;
using UnityEngine;
using UnityEngine.AI;

namespace FSM
 {
     public abstract class MoveState : BaseState
     {
         protected override void StartState()
         {
         }

         private Vector3 _prevPoint;
         public bool HaveTarget;
         public bool RoadFinished = false;
         
         public const float RePathAfterDistanceChanged = 2.0f;

         protected override void InState()
         {
             base.InState();
        
         }

         private void ChangeDirection()
         {
             
         }

         protected void MoveToTarget(Vector3 position, float minDistace)
         {
          
         }
     
         protected void RoadEnded()
         {
        
         }
         
         protected abstract bool TryCheckBlock(Vector3 direct, out BaseUnit unit);

         protected override void FinishState()
         {
         }
         
         public override void ClearData()
         {

         }

     }
 }