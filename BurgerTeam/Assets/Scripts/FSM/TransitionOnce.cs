namespace FSM
{
	public class TransitionOnce : Transition {
		private bool canTrigger;

        private TransitionOnce(StateId targetState, TransitionID name) 
			: base(targetState, name) {
			canTrigger = true;
		}
		
		public override bool IsTriggered () {
			if (!canTrigger) {
				return false;
			}
			
			bool triggered = base.IsTriggered ();
			if(triggered) {
				canTrigger = false;
				return true;
			}
			
			return false;
		}

        public new static Transition ToState(StateId targetState, TransitionID name)
        {
			return new TransitionOnce(targetState, name);	
		}
	}
}