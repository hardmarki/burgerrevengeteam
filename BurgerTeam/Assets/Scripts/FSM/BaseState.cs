﻿using System;
using System.Collections.Generic;

namespace FSM
{
	public abstract class BaseState
	{
		public abstract StateId Id { get; }

		public abstract List<EventID> InterruptEvents { get; }
		public abstract EventID TransitionEventId { get; }

		private Action _actions;
		private Action _entryActions;
		private Action<StateId> _exitActions;
		private readonly List<Transition> _transitions = new List<Transition>();

		public Action EntryActions => _entryActions;

		public Action CurrentActions => _actions;

		public Action<StateId> ExitActions => _exitActions;

		public IList<Transition> Transitions => _transitions;

		public StateMachine Machine { protected get; set; }

		public Transition GetTransitionEvents()
		{
			 return TransitionEvent.ToState(Id, (TransitionID)Id).OnEvents(TransitionEventId);
		}

		public BaseState()
		{
			_entryActions = StartState;
			_actions = InState;
			_exitActions = FinishState;
		}

		public virtual void InitStates()
		{
			
		}

		public BaseState ClearActions()
		{
			_entryActions = null;
			_actions = null;
			_exitActions = null;
			_transitions.Clear();
			return this;
		}

		public virtual void ClearData()
		{
		}
		
		private BaseState AddTransitionsAfter(Transition transition)
		{
			this._transitions.Add(transition);
			return this;
		}

	
		public bool RejectEvent(EventID eventName)
		{
			foreach (var interruptEvent in InterruptEvents)
			{
				if (eventName == interruptEvent)
				{
					return false;
				}
			}

			return InterruptEvents.Count > 0;
		}

		public bool IsInterruptable()
		{
			return InterruptEvents.Count == 0;
		}

		public override string ToString()
		{
			return $"'{Id}' interrupt event '{InterruptEvents}'";
		}

		protected virtual void StartState()
		{
		}

		protected virtual void InState() 
		{
		}

		protected virtual void FinishState() 
		{
		}
		
		protected virtual void FinishState(StateId toState)
		{
			FinishState();
		}

		protected void PushEvent(EventID eventId)
		{
			Machine.PushEvent(eventId);
		}
	}
}
