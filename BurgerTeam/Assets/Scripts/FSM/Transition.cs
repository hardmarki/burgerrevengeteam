using System;
using System.Collections.Generic;

namespace FSM
{
    public enum TransitionID:int
    {
        None = 0,
        toDeath,
        toGotHit,
        toSpecial,
        toIdle,
        toClick,
        toAttack,
        toStun,
		toHold,
		idleToAttack,
		idleToSpecial,
		toCrit,
		idleToCrit,
        toFreeze,
		attackToCrit,
		toDodge,
		toBlock,
        toFunIdle,
		freezeToIdle,
		stunToIdle,
        toMove,
        toHoldMove,
        moveToUnitMove,
        moveToAttackMove
    }

	public class Transition
	{
        public StateId targetState;
		public List<Action> actionsList = new List<Action>();
        public TransitionID name;
		
		public Scanner Scanner;

        protected Transition(StateId targetState, TransitionID name = TransitionID.None)
        {
			this.targetState = targetState;
			this.name = name;
		}
		
		public virtual bool IsTriggered() {
			return Scanner.Test();
		}
		
        public static Transition ToState(StateId targetState, TransitionID name = TransitionID.None)
        {
			return new Transition(targetState, name);	
		}
		
		public override string ToString () {
			return $"Transition '{name}' to state [{targetState}]";
		}

	}
	
}

