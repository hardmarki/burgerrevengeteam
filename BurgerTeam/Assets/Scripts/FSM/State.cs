namespace FSM {

    public enum StateId:byte
    {
        None = 0,
        Death,
        Idle,
        Attack,
        Spawn,
        Move,
		Stun,
        Build,
        Resurrect,
        Scared,
        Cast,
        Uncontroll,
        AiSpecial
    }
}

