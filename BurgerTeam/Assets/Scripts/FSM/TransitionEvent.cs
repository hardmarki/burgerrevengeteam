namespace FSM
{
	[System.Flags]
    public enum EventID : byte
    {
        None,
        OnDeath,
        OnIdle,
        OnAttack,
        OnMove,
		OnSpawn,
		OnStun,
	    OnBuild,
	    OnResurrect,
	    OnScared,
	    OnCast,
	    OnUncontroll,
	    OnAiSpecial
    }

	public sealed class TransitionEvent : Transition {
		private class EventScanner : Scanner {
			private readonly TransitionEvent _transition;
            private readonly EventID _eventName;

            public EventScanner(TransitionEvent transition, EventID eventName)
            {
				_transition = transition;
				_eventName = eventName;
			}
			
			public override bool Test() {
				return _eventName == _transition._triggeredEvent;
			}
		}

        private EventID _eventNames;
        private EventID _triggeredEvent;

        private TransitionEvent(StateId targetState, TransitionID name)
			:base(targetState, name) {
		}

        public bool IsTriggered(EventID eventName)
        {
			_triggeredEvent = eventName;
			return IsTriggered();
		}

        public Transition OnEvents(EventID eventNames)
        {
            _eventNames = eventNames;
            Scanner = new EventScanner(this, eventNames);
			return this;
		}

        public new static TransitionEvent ToState(StateId targetState, TransitionID name)
        {
			return new TransitionEvent(targetState, name);	
		}

		public override string ToString() {

            return $"Transition to state {targetState} on events {_eventNames}";
		}
	}
}

