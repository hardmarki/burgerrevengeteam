﻿namespace Units
{
	public interface IAttackCondition
	{
		void FindTarget();
		bool HaveInAttackRange();
		BaseUnit CurrentTarget { get; }
		bool Head { get; }
	}
	
	public interface ICondition:IAttackCondition
	{
		bool HaveInVisibleRange();
		BaseUnit ClosestVisibleUnit { get; }
	}

	public class EmptyCondition : ICondition
	{
		public void FindTarget()
		{
		}

		public bool HaveInAttackRange()
		{
			return false;
		}

		public bool HaveInVisibleRange()
		{
			return false;
		}

		public BaseUnit CurrentTarget => null;

		public bool Head => false;

		public BaseUnit ClosestVisibleUnit => null;
	}
}