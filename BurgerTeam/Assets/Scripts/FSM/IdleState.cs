﻿using System;
using System.Collections.Generic;

namespace FSM
{
    public class IdleState : BaseState {
        public override StateId Id => StateId.Idle;

        private static readonly List<EventID> EventIds = new List<EventID>() {EventID.OnDeath, EventID.OnMove, EventID.OnAttack, EventID.OnStun, EventID.OnUncontroll};

        public override List<EventID> InterruptEvents => EventIds;

        public override EventID TransitionEventId => EventID.OnIdle;

        private readonly Action _onStart;
        public IdleState(Action onStart)
        {
            _onStart = onStart;
        }
        
        protected override void StartState()
        {
            _onStart?.Invoke();
        }
    }
}
