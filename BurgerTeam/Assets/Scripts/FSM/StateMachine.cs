using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
	public sealed class StateMachine : MonoBehaviour
	{
		private bool _logEnable;
		private string _unitName;

		private readonly List<TransitionEvent> _transitionEvents = new List<TransitionEvent>();

		public readonly Dictionary<EventID, object[]> EventsParams = new Dictionary<EventID, object[]>();

		public T Get<T>(EventID id, int index = 0)
		{
			object[] obj;
			if (!EventsParams.TryGetValue(id, out obj)) return default(T);
			if (obj == null || obj.Length <= index) return default(T);
			var t = (T) obj[index];
			if (t != null)
			{
				return t;
			}
			return default(T);
		}

		public List<BaseState> States { get; } = new List<BaseState>(12);

		[SerializeField] private EventID[] _events = new EventID[5];


		public static StateMachine Create(GameObject go, BaseState state)
		{
			var m = Create(go);
			m.AddState(state);
			m.SetStartState(state.Id);
			return m;
		}

		private StateId _startStateId;
		public void SetStartState(StateId id)
		{
			_startStateId = id;
			CurrentState = GetStateByName(id);
		}

		public static StateMachine Create(GameObject go)
		{
			var m = go.AddComponent<StateMachine>();
			return m;
		}
		
		public void Rebind()
		{
			ClearEvents();
			CurrentState = States[0];
			foreach (var s in States)
			{
				s.ClearActions();
			}
		}

		public void Reset()
		{
			ClearEvents();
			CurrentState = GetStateByName(_startStateId);
			foreach (var s in States)
			{
				s.ClearData();
			}
		}

		public StateMachine AddStates(params BaseState[] states)
		{
			foreach (var s in states)
			{
				AddState(s);
			}

			return this;
		}

		public StateMachine AddState(BaseState state)
		{
			state.Machine = this;
			States.Add(state);
			AddEventTransition(state.GetTransitionEvents());
			return this;
		}

		public void RemoveState(StateId state)
		{
			if (state == StateId.Idle)
			{
				return;
			}

			var found = States.Find((s) => s.Id == state);
			if (found == null) return;
			if (found == CurrentState)
			{
				CurrentState = GetStateByName(StateId.Idle);
			}

			States.Remove(found);
		}

		public void InitStates()
		{
			if (CurrentState == null)
				CurrentState = States[0];
			foreach (var s in States)
			{
				s.InitStates();
			}
		}

		public StateMachine AddEventTransitions(IEnumerable<Transition> transitionEvents)
		{
			foreach (var t in transitionEvents)
			{
				AddEventTransition(t as TransitionEvent);
			}

			return this;
		}

		public StateMachine AddEventTransition(Transition t)
		{
			_transitionEvents.Add(t as TransitionEvent);
			return this;
		}

		public void RunActions()
		{
			UpdateState();
		}

		public bool HasEvent(EventID eventName)
		{
			foreach (var e in _events)
			{
				if (e == eventName)
				{
					return true;
				}
			}
			return false;
		}

		public void PushEvent(EventID eventName, params object[] arg)
		{
			EventsParams[eventName] = arg;
			PushEvent(eventName);
		}

		public void PushEvent(EventID eventName)
		{
			if (HasEvent(eventName))
				return;

			if (!CurrentState.InterruptEvents.Contains(eventName))
			{
				return;
			}

			if (_logEnable)
			{
			
			}

			for (var i = 0; i < _events.Length; i++)
			{
				if (_events[i] != EventID.None) continue;
				_events[i] = eventName;
				return;
			}

			//  _events.Add(eventName);
		}

		private void ClearEvent(EventID eventName)
		{
			for (var i = 0; i < _events.Length; i++)
			{
				if (_events[i] == eventName)
				{
					_events[i] = EventID.None;
				}
			}
		}

		private void ClearEvents()
		{
			for (var i = 0; i < _events.Length; i++)
			{
				_events[i] = EventID.None;
			}
		}

		public BaseState GetStateByName(StateId stateName)
		{
			foreach (var state in States)
			{
				if (stateName == state.Id)
				{
					return state;
				}
			}

			return null;
		}

		public int TimeIn { get; private set; }

		public StateMachine RemoveTransitionEvent(TransitionID name)
		{
			var i = 0;
			while (i < _transitionEvents.Count)
			{
				var transitionEvent = _transitionEvents[i];
				if (transitionEvent.name.Equals(name))
				{
					_transitionEvents.RemoveAt(i);
					return this;
				}

				i++;
			}

			return this;
		}

		public void RemoveAllTransitionEvent()
		{
			_transitionEvents.Clear();
		}
		
		private void UpdateState()
		{
			if (Sender)
			{
				TimeIn++;
				// find if there is transition triggered by event or transition of current state
				var triggeredTransition = FindTransitionEvent() ?? FindCurrentStateTransition();

				// add actions for triggered transition
				if (triggeredTransition != null)
				{
					var targetState = GetStateByName(triggeredTransition.targetState);
					CurrentState.ExitActions(targetState.Id);
					foreach (var trAction in triggeredTransition.actionsList)
					{
						trAction();
					}

					targetState.EntryActions();
					CurrentState = targetState;
				}
			}
			CurrentState.CurrentActions();
		}

		private Transition FindTransitionEvent()
		{
			for (var i = 0; i < _events.Length; i++)
			{
				if (_events[i] == EventID.None)
					continue;
				// check if this event will interrupt current state
				if (CurrentState.RejectEvent(_events[i]))
				{
					continue;
				}

				foreach (var transitionEvent in _transitionEvents)
				{
					if (!transitionEvent.IsTriggered(_events[i]))
						continue;
					ClearEvent(_events[i]);
					return transitionEvent;
				}
			}
			return null;
		}

		private Transition FindCurrentStateTransition()
		{
			// skip not interruptable states
			if (CurrentState.IsInterruptable())
			{
				return null;
			}

			for (var i = 0; i < CurrentState.Transitions.Count; i++)
			{
				if (CurrentState.Transitions[i].IsTriggered())
				{
					return CurrentState.Transitions[i];
				}
			}
			return null;
		}

		public override string ToString()
		{
			return $"State: {CurrentStateId}, events: {_events.Length}";
		}

		public StateId CurrentStateId => CurrentState.Id;

		public BaseState CurrentState { get; private set; }

		public bool IsState(StateId stateid)
		{
			return CurrentStateId == stateid;
		}
		
		public bool Sender { get; set; } = true;
	}
}

