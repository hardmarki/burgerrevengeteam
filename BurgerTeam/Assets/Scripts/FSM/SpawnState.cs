﻿using System;
using System.Collections.Generic;

namespace FSM
{
    public class SpawnState : BaseState {
        public override StateId Id => StateId.Spawn;

        private static readonly List<EventID> EventIds = new List<EventID>() { EventID.OnDeath, EventID.OnIdle };

        public override List<EventID> InterruptEvents => EventIds;

        public override EventID TransitionEventId => EventID.OnSpawn;

        private int _time;
        private readonly int _timeSpawn;
        
        private readonly Action _onSpawn;
        
        public SpawnState(int tickTime, Action onSpawn)
        {
            _time = _timeSpawn = tickTime;
            _onSpawn = onSpawn;
        }

        protected override void StartState()
        {
            base.StartState();
            _time = _timeSpawn;
        }

        protected override void InState()
        {
            if (_time-- > 0) return;
            PushEvent(EventID.OnIdle);
            _onSpawn();
        }

        public override void ClearData()
        {
            base.ClearData();
            _time = _timeSpawn;
        }
    }
}
