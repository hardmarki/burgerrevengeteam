﻿using System.Collections;

namespace BurgerTeam.JsonConfigs
{ 
    public class TestConfig : IJsonConfig
    {   
        public string Name
        {
            get;
            set;
        }

        public string Test
        {
            get;
            set;
        } 

        public void Read(Hashtable table)
        {
            
            this.Test = table["Name"] as string;
        }
    }
}
