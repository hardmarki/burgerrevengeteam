﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BurgerTeam.Pooling;

namespace BurgerTeam.Spawners
{
    public class BaseSpawner : MonoBehaviour
    {
        [SerializeField] private AbstractFactory _enemyFactory = null;
        [SerializeField] private Transform _spawnPoint = null;
        
        private List<EnemyController> _spawnedCollection = new List<EnemyController>();

        private void Update()
        {
            if (Input.GetKey(KeyCode.Tab))
                Spawn();
            if (Input.GetKey(KeyCode.LeftAlt))
                DespawnAll();
        }


        public void Spawn()
        {
            var enemy = _enemyFactory.Spawn<EnemyController>(_spawnPoint.position, transform);
            enemy.TestInit();
            
            _spawnedCollection.Add(enemy);
        }

        public void DespawnAll()
        {
            if (_spawnedCollection.Count == 0)
                return;
        
            foreach (var enemyController in _spawnedCollection)
                PrefabPool.Instance.Despawn(enemyController.gameObject);
            _spawnedCollection.Clear();
        }
    }
}
