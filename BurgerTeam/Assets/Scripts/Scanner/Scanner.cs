﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScanner
{
    bool HaveTarget();
    void FindTarget();
}

public abstract class Scanner: IScanner
{
    public BaseUnit CurrentTarget = null;
    public abstract void FindTarget();
    protected abstract bool ShouldChangeTarget();
    protected abstract void GetTarget();
    
    public bool HaveTarget()
    {
        return CurrentTarget != null;
    }
}

public class EmptyScanner : Scanner
{
    public override void FindTarget()
    {
        CurrentTarget = null;
    }

    protected override bool ShouldChangeTarget()
    {
        return false;
    }

    protected override void GetTarget()
    {
        
    }
}

public class BaseScanner : Scanner
{
    public override void FindTarget()
    {
        if(ShouldChangeTarget())
            GetTarget();
    }

    protected override bool ShouldChangeTarget()
    {
        return false;
    }

    protected override void GetTarget()
    {
        
    }
}

public class RaycastScanner : BaseScanner
{
    private Ray _ray;

    private LayerMask _layerMask;
    
    public RaycastScanner(Ray ray, LayerMask mask)
    {
        _ray = ray;
        _layerMask = mask;
    }

    protected override void GetTarget()
    {
        if (Physics.Raycast(_ray, out var target, _layerMask))
        {
            var t = target.collider.gameObject.GetComponent<BaseUnit>();
            // t == null acceptable
            CurrentTarget = t;
        }
        else
        {
            CurrentTarget = null;
        }
    }

    protected override bool ShouldChangeTarget()
    {
        return true;
    }
}
