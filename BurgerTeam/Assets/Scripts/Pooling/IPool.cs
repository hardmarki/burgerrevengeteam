﻿
namespace BurgerTeam.Pooling
{
    public interface IPool<T>
    {
        void Preload(int count);
        T Spawn();
        void Despawn(T instance);
        void Clear();
    }
}

