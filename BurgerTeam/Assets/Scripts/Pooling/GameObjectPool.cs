﻿using UnityEngine;

namespace BurgerTeam.Pooling
{

    public class GameObjectPool : BasePool<GameObject>
    {
        private GameObject prefab;
        private Transform parent;

        public GameObjectPool(GameObject prefab) : base()
        {
            this.prefab = prefab;
        }

        public GameObjectPool(GameObject prefab, Transform parent) : base()
        {
            this.prefab = prefab;
            this.parent = parent;
        }

        protected override GameObject CreateInstance()
        {
            GameObject instance = GameObject.Instantiate(this.prefab, this.parent);
            instance.SetActive(false);
            return instance;
        }

        protected override void DestroyInstance(GameObject instance)
        {
            GameObject.Destroy(instance);
        }

        protected override void ProcessSpawned(GameObject instance)
        {
            instance.transform.SetParent(null);
            instance.SetActive(true);
        }

        protected override void ProcessDespanwned(GameObject instance)
        {
            instance.SetActive(false);
            instance.transform.SetParent(this.parent);
        }
    }

}