﻿using System.Collections.Generic;
using UnityEngine;

namespace BurgerTeam.Pooling
{
    public class PrefabPool
    {
        private Dictionary<GameObject, GameObjectPool> pools;
        private Dictionary<GameObject, GameObjectPool> usedPools;
        private Transform parent;

        private static PrefabPool _instance;
        public static PrefabPool Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PrefabPool();
                }
                return _instance;
            }
        }

        private PrefabPool()
        {
            this.pools = new Dictionary<GameObject, GameObjectPool>();
            this.usedPools = new Dictionary<GameObject, GameObjectPool>();

            GameObject parentObject = new GameObject("Prefab Pool");
            this.parent = parentObject.transform;
        }

        private GameObjectPool GetPool(GameObject prefab)
        {
            GameObjectPool pool = null;
            if (!this.pools.TryGetValue(prefab, out pool))
            {
                GameObject parentObject = new GameObject(string.Format("{0} Pool", prefab.name));
                parentObject.transform.SetParent(this.parent);

                pool = new GameObjectPool(prefab, parentObject.transform);
                this.pools.Add(prefab, pool);
            }
            return pool;
        }

        public void Preload(GameObject prefab, int count)
        {
            GameObjectPool pool = this.GetPool(prefab);
            pool.Preload(count);
        }

        public GameObject Spawn(GameObject prefab)
        {
            GameObjectPool pool = this.GetPool(prefab);
            GameObject instance = pool.Spawn();
            prefab.transform.position = Vector3.zero;
            this.usedPools.Add(instance, pool);
            return instance;
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            GameObject instance = this.Spawn(prefab);
            instance.transform.SetPositionAndRotation(position, rotation);
            return instance;
        }

        public GameObject Spawn(GameObject prefab, Transform parent)
        {
            GameObject instance = this.Spawn(prefab);
            instance.transform.SetParent(parent);
            return instance;
        }

        public void Despawn(GameObject instance)
        {
            GameObjectPool pool = null;
            if (this.usedPools.TryGetValue(instance, out pool))
            {
                pool.Despawn(instance);
                this.usedPools.Remove(instance);
            }
            else
            {
                Debug.LogWarning(string.Format("{0} is not from the pool, destroying.", instance.name));
                GameObject.Destroy(instance);
            }
        }
    }
}