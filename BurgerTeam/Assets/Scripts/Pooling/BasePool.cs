﻿using System.Collections.Generic;

namespace BurgerTeam.Pooling
{
    public abstract class BasePool<T> : IPool<T>
    {
        private readonly Queue<T> freeObjects = new Queue<T>();

        public void Preload(int count)
        {
            for (int i = 0; i < count; i++)
            {
                T instance = this.CreateInstance();
                this.freeObjects.Enqueue(instance);
            }
        }

        public void Clear()
        {
            while (this.freeObjects.Count > 0)
            {
                T instance = this.freeObjects.Dequeue();
                this.DestroyInstance(instance);
            }
        }

        public void Despawn(T instance)
        {
            this.ProcessDespanwned(instance);
            this.freeObjects.Enqueue(instance);
        }

        public T Spawn()
        {
            T instance;
            if (this.freeObjects.Count > 0)
            {
                instance = this.freeObjects.Dequeue();
            }
            else
            {
                instance = this.CreateInstance();
            }
            this.ProcessSpawned(instance);
            return instance;
        }

        protected abstract T CreateInstance();
        protected abstract void DestroyInstance(T instance);
        protected virtual void ProcessDespanwned(T instance) { }
        protected virtual void ProcessSpawned(T instance) { }
    }

}
