﻿using System.Collections.Generic;
using UnityEngine;

namespace BurgerTeam.LevelGeneration
{
    public class LevelPiece : MonoBehaviour
    {
        [SerializeField]
        public int weight = 1;
        [SerializeField]
        private List<Transform> entrances;

        [SerializeField]
        private List<Transform> exits;

        public bool IsMatch(LevelPiece levelPiece)
        { 
            if (levelPiece.entrances.Count < this.exits.Count)
            {
                return false;
            } 
            else
            {
                Transform firstExit = this.exits[0];
                Transform firstEntrance = levelPiece.entrances[0];
                Vector3 offset = firstEntrance.position - firstExit.position;

                for(int i = 0; i < this.exits.Count; i++)
                {
                    if ((this.exits[i].position + offset) != levelPiece.entrances[i].position)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public Vector3 GetExitPosition()
        {
            return this.exits.Count > 0 ? this.exits[0].position : Vector3.zero;
        }

        public Vector3 GetEntrancePosition()
        {
            return this.entrances.Count > 0 ? this.entrances[0].position : Vector3.zero;
        }
    }
}
