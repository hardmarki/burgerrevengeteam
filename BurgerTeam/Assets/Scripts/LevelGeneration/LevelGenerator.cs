﻿

using BurgerTeam.Pooling;
using System.Collections.Generic;
using UnityEngine;
namespace BurgerTeam.LevelGeneration
{
    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField]
        private List<LevelPiece> pieces;
        
        [SerializeField]
        private int preload;

        [SerializeField]
        private LevelPiece lastPiece;

        private WeightedRandomizer<LevelPiece> randomizer;

        private void Start()
        {
            foreach(LevelPiece piece in this.pieces)
            {
                PrefabPool.Instance.Preload(piece.gameObject, this.preload);
            }
            this.InitRandomizer();

            for (int i = 0; i < this.preload; i++)
            {
                LevelPiece nextPiece = this.randomizer.TakeOne((piece) => { return this.lastPiece == null || this.lastPiece.IsMatch(piece); });
                this.SpawnPiece(nextPiece);
            }
        }

        private void InitRandomizer()
        {
            Dictionary<LevelPiece, int> piecesDictionary = new Dictionary<LevelPiece, int>();
            foreach(LevelPiece levelPiece in this.pieces)
            {
                piecesDictionary.Add(levelPiece, levelPiece.weight);
            }
            this.randomizer = WeightedRandomizer.From(piecesDictionary);

        }

        private void SpawnPiece(LevelPiece levelPiece)
        {   
            LevelPiece newPiece = PrefabPool.Instance.Spawn(levelPiece.gameObject, this.transform).GetComponent<LevelPiece>();
            Vector3 exitPosition = Vector3.zero;
            if (this.lastPiece != null)
            {
                exitPosition = this.lastPiece.GetExitPosition();
            }

            Vector3 startPosition = newPiece.GetEntrancePosition();
            Vector3 offset = exitPosition - startPosition;
            newPiece.transform.position += offset;
            this.lastPiece = newPiece;
        }

    }

}
