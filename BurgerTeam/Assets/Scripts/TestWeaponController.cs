﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWeaponController : MonoBehaviour
{
    private bool _attackPermitted;

    public event Action OnAction;
    
    private float timer = 1.0f;
    public void Attack(bool isPermitted, Action onAttack = null)
    {
        if (_attackPermitted != isPermitted)
            timer = 1.0f;
        _attackPermitted = isPermitted;

        OnAction = onAttack;
    }

    private float counter;
    private void Update()
    {
        if (_attackPermitted)
        {
            if (counter <= 0)
            {
                counter = timer;
                OnAction?.Invoke();
            }
            counter -= Time.deltaTime;
        }
    }

}
