﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUnit : MonoBehaviour
{
    private IMovement _movement;

    //TODO: StateMachine (Spawn, Idle, Death)

    private StatsContainer _stats;

    public HitStat HP;

    public BaseStat Weight => _stats.Get(StatType.Weight);
    
    public bool IsDead => HP!=null && HP.Value <= 0;

    public virtual void Init()
    {
        // TODO: Get stats for configs
        // TODO : Create movement with Factory

        //HP = _stats.Get(StatType.Hit) as HitStat;
        if (HP == null)
        {
            HP = new HitStat(2);
        }

    }

   
    
}
