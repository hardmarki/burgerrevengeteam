﻿


using System;
using System.Collections.Generic;
using UnityEngine;

namespace BurgerTeam.JsonConfigs
{
    public class ConfigsManager : Singleton<ConfigsManager>
    {
        private Dictionary<Type, IJsonConfigContainer> containers = new Dictionary<Type, IJsonConfigContainer>();
        private string configsPath
        {
            get
            {
                return Application.streamingAssetsPath + "/Configs";
            }
        }

        public void Init()
        {
            this.AddContainer<TestConfig>("Test");
            this.Load();
        }

        public void AddContainer<T>(string folderName) where T : IJsonConfig, new()
        {
            this.containers.Add(typeof(T), new ConfigContainer<T>(folderName));
        }

        public void Load()
        {
            foreach (IJsonConfigContainer container in this.containers.Values)
            {
                container.Load(this.configsPath);
            }
        }

        public T Get<T>(string name)
        {
            Type type = typeof(T);
            if (this.containers.ContainsKey(type))
            {
                return (T)this.containers[type].GetConfig(name);
            }
            return default(T);
        }
    }
}
