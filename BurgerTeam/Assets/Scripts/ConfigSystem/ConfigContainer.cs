﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MiniJSON;

namespace BurgerTeam.JsonConfigs
{
    public interface IJsonConfigContainer
    {
        void Load(string path);
        IJsonConfig GetConfig(string configName);
    }

    public class ConfigBuilder<T> where T : IJsonConfig, new()
    {
        public T Build(Dictionary<string, object> dict)
        {
            T result = default(T);
            if (dict.Keys.Count > 0)
            {
                result = new T();
                result.Name = dict.Keys.First();
                Dictionary<string, object> table = dict[result.Name] as Dictionary<string, object>;
                result.Read(new Hashtable(table));
            }
            return result;
        }
    }

    public class ConfigContainer<T> : IJsonConfigContainer where T : IJsonConfig,new()
    {
        private Dictionary<string, IJsonConfig> configs = new Dictionary<string, IJsonConfig>();
        private string folderName;
        private ConfigBuilder<T> builder = new ConfigBuilder<T>();

        public ConfigContainer(string folderName)
        {
            this.folderName = folderName;
        }

        public void Load(string path)
        {
            string fullPath = Path.Combine(path, this.folderName);
            DirectoryInfo d = new DirectoryInfo(fullPath);
            FileInfo[] Files = d.GetFiles("*.json");
            foreach (FileInfo file in Files)
            {
                string jsonString = File.ReadAllText(file.FullName);
                Dictionary<string, object> dict = Json.Deserialize(jsonString) as Dictionary<string, object>;
                T config = this.builder.Build(dict);
                this.configs.Add(config.Name, config);
            }
        }

        public IJsonConfig GetConfig(string configName)
        {
            IJsonConfig result = null;
            this.configs.TryGetValue(configName, out result);
            return result;
        }
    }
}