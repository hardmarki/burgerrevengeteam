﻿using System.Collections;
using System.Collections.Generic;

namespace BurgerTeam.JsonConfigs
{
    public interface IJsonConfig 
    {
        string Name { get; set; }
        void Read(Hashtable table);
    }
}

