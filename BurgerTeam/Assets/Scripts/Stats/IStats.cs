﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IStats 
{
    
}

public interface IDynamicStat
{
    void Add(int value);
    void Remove(int value);
}
