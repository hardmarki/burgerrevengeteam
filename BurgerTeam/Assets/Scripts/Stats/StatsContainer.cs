﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StatsContainer
{
   public List<BaseStat> Stats = new List<BaseStat>();

   public virtual void Init()
   {
      Stats.Add(new HitStat(1,1));
   }

   public BaseStat Get(StatType type)
   {
      return Stats.FirstOrDefault(x => x.Type == type);
   }
}
