﻿public enum StatType : byte
{
    None = 0,
    Hit = 1,
    HorizontalSpeed = 2,
    Weight = 3
}

public class BaseStat : IStats
{
    public int Value;
    public int MaxValue;

    public StatType Type = StatType.None;

    public BaseStat(int basic)
    {
        Value = MaxValue = basic;
    }

    public BaseStat(int basic, int max)
    {
        Value = basic;
        MaxValue = max;
    }
}
