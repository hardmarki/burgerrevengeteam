﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitStat : BaseStat, IDynamicStat
{
    public float Percent => 1f * Value / MaxValue;

    public bool IsDead => Value <= 0;

    public UnityEvent OnChange;

    public HitStat(int basic) : base(basic)
    {
        Type = StatType.Hit;
    }

    public HitStat(int basic, int max) : base(basic, max)
    {
        Type = StatType.Hit;
    }

    public void Add(int value)
    {
        Value = Mathf.Max(Value + value, MaxValue);
       OnChange?.Invoke();
    }

    public void Remove(int value)
    { 
        Value = Mathf.Min(0, Value - value);
        OnChange?.Invoke();
    }
}
