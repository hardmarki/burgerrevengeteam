﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BaseBullet : MonoBehaviour
{
   // TODO: Create target type
   private int PhysLayer;

   [SerializeField]private Rigidbody _rigidbody;
   
   private void Start()
   {
    
   }

   public void Play(Vector3 startPos, Vector3 direction, float speed)
   {
      _rigidbody = gameObject.AddComponent<Rigidbody>();
      _rigidbody.useGravity = false;
      
      gameObject.transform.position = startPos;
      _rigidbody.AddForce(_rigidbody.mass * speed * direction, ForceMode.Impulse);
   }

   private void OnCollisionEnter(Collision other)
   {
      Destroy(gameObject);
   }
}
