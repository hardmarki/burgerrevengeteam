﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurgerController : UnitController
{
    [SerializeField] private PhysicMovement Movement;
    [SerializeField] private Inventory inventory;
    [SerializeField] private BurgerAnimator animator;

    private void Awake()
    {
        Movement.SetWings(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("PowerUP"))
        {
            //if ( == "Bacon")
            {
                inventory.SetItem(other.gameObject.GetComponent<Item>().ItemId, true);
                Destroy(other.gameObject);
            }
        }
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Movement.Jump())
                animator.StartJump();
        }
        animator.SetFly(inventory.Have(InventoryItems.Bacon) && Input.GetKey(KeyCode.Space) && !Movement.OnGround());
        animator.SetGround(Movement.OnGround());
    }
}
