﻿using System;
using System.Collections;
using System.Collections.Generic;
using FSM;
using UnityEngine;
using UnityEngine.Serialization;
using Object = System.Object;

public class UnitController : MonoBehaviour
{
   public BaseUnit Unit => _unit;

   protected BaseUnit _unit;

   protected StateMachine Machine; 
   
  public WeaponController activeWeaponController;

   protected IScanner Scanner;

   // Init from factory
   public virtual void Init(BaseUnit unit)
   {
      _unit = unit;
         // Register in container
         InitStateMachine();
   }

   protected virtual void InitStateMachine()
   {
      Machine = StateMachine.Create(gameObject);
      BuilStateMachine();
      Machine.InitStates();
      Scanner = new EmptyScanner();
   }

   protected virtual void BuilStateMachine()
   {
      var idle = new IdleState(() => { });
      var death = new DeathState(() => { }, () => { }, 1);
      var spawn = new SpawnState(1, () => { });
      Machine.AddStates(idle, death, spawn);
      Machine.SetStartState(StateId.Spawn);
   }
   public virtual void FixedUpdate()
   {
        return;
      if (!Unit.IsDead && Machine != null)
      {
         Machine.RunActions();
         Scanner.FindTarget();
      }
      
   }

   public virtual void Death()
   {
      // Rework
      Destroy(gameObject);
   }

   public virtual void GetHit(int value = 1)
   {
      if(Unit.IsDead)
      {
         Death();
         return;
      }
      Debug.Log("once");
      Unit.HP.Remove(value);
   }

}
