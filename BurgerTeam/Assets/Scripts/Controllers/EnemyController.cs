﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyController : UnitController, ISpawnedObject
{
    private void Start()
    {
        var unit = new BaseUnit(); //why this way (not AddComponent<>)
        unit.Init();
        Init(unit);
    }

    // Init from factory
    public virtual void Init(BaseUnit unit) //why this is virtual not override ?
    {
        base.Init(unit);
        Scanner = new RaycastScanner(new Ray(gameObject.transform.position, Vector3.left * 10) , 1 << LayerMask.NameToLayer("Burger") );
        
        var weaponController = gameObject.AddComponent<TestWeaponController>();
        weaponController.Attack(true, () => { Attack();});
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
   
        attack =Scanner.HaveTarget();
    }

    private bool attack;
    private void Attack()
    {
        if (attack)
        {
            var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            var bullet = go.AddComponent<BaseBullet>();
            bullet.Play(gameObject.transform.position + Vector3.left / 10, Vector3.left, 50);
        }
    }
    
    public void TestInit()
    {
        Debug.Log($"{gameObject.name} was initialized");
    }
    private void OnCollisionEnter(Collision other)
    {
        var unit = other.gameObject.GetComponent<BaseUnit>();
        if(other.collider.gameObject.layer == (LayerMask.NameToLayer("Burger")) && _unit != null)
        { 
            foreach (var cntc in other.contacts)
            {
                if (cntc.point.y > gameObject.transform.position.y)
                {
                    GetHit(_unit.Weight.Value);
                }
            }
        }
    }
}
