﻿using BurgerTeam.JsonConfigs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    
    private bool _isGround;

    private bool _isWings = false;

    private void Awake()
    {
        ConfigsManager.Instance.Init();
        gameObject.AddComponent<VerticalBoosterHandler>().Init(_rigidbody);
    }


    private void LateUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.velocity = _rigidbody.velocity.y * Vector3.up + (-Vector3.right);
        }
        if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.velocity = _rigidbody.velocity.y * Vector3.up + (Vector3.right);
        }
      

    }
        
    public bool OnGround()
    {
        return _isGround;
    }

    public bool Jump()
    {
        if (OnGround())
        {
            _rigidbody.AddForce(Vector3.up * 25, ForceMode.Impulse);
            _isGround = false;
        }
        return !_isGround;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 1 << LayerMask.NameToLayer("Enemy"))
        {
            Jump();
            return;
        }

        if (!_isGround)
            _isGround = true;
    }

    public void SetWings(bool enable)
    {
        _isWings = enable;
    }
}
