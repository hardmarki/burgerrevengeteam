﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
   void Init(BaseStat speedStat);
   bool OnGround();
}
