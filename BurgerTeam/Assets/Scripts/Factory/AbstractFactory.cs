﻿using UnityEngine;

public abstract class AbstractFactory : MonoBehaviour
{
    public void Spawn(Vector3 position, Transform parent = null, int spawnedObjectId = 0 )
        => GetSpawnedObject(position, parent, spawnedObjectId);

    public T Spawn<T>(Vector3 position, Transform parent = null, int spawnedObjectId = 0) where T : ISpawnedObject
        => (T)GetSpawnedObject(position, parent, spawnedObjectId);
    
    protected abstract ISpawnedObject GetSpawnedObject(Vector3 position, Transform parent, int spawnedObjectId);

}
