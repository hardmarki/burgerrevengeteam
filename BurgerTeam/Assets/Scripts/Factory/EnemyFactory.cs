﻿using System.Collections;
using System.Collections.Generic;
using BurgerTeam.Pooling;
using UnityEngine;

public class EnemyFactory : AbstractFactory
{
    [SerializeField] private EnemyController _enemyControllerPrefab;
    
    protected override ISpawnedObject GetSpawnedObject(Vector3 position, Transform parent, int spawnedObjectId)
    {
        var go = PrefabPool.Instance.Spawn(_enemyControllerPrefab.gameObject, parent).GetComponent<EnemyController>();
        go.transform.position = position;
        return go;
    }
}
