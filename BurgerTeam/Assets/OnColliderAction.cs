﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnColliderAction : MonoBehaviour
{
    private UnityAction onTriggerEnter;
    private UnityAction onCollisionEnter;
    private UnityAction onTriggerExit;
    private UnityAction onCollisionExit;

    public Collider Collider;

    private void Start()
    {
        Collider = GetComponent<Collider>();
    }

    public void OnTrigerEnterInit(UnityAction act)
    {
        onTriggerEnter = new UnityAction(act);
    }

    public void OnCollisionEnterInit(UnityAction act)
    {
        onCollisionEnter = new UnityAction(act);
    }
    public void OnTrigerExitInit(UnityAction act)
    {
        onTriggerExit = new UnityAction(act);
    }

    public void OnCollisionExitInit(UnityAction act)
    {
        onCollisionExit = new UnityAction(act);
    }

    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter?.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        onTriggerExit?.Invoke();
    }

    private void OnCollisionEnter(Collision collision)
    {
        onCollisionEnter?.Invoke();
    }

    private void OnCollisionExit(Collision collision)
    {
        onCollisionExit?.Invoke();
    }

    public void Clear()
    {
        onCollisionEnter = null;
        onCollisionExit = null;
        onTriggerEnter = null;
        onTriggerExit = null;
    }
}
