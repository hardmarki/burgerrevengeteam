﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurgerAnimator : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public void SetFly(bool enable)
    {
        animator.SetBool("Fly", enable);
    }

    public void StartJump()
    {
        animator.SetTrigger("InJump");
    }
    public void SetGround(bool enable)
    {
        animator.SetBool("Ground", enable);
    }

}
